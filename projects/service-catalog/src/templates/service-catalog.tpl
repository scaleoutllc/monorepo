<!doctype html>
<html>
<head>
  <title>Service Catalog</title>
</head>
<body>
<h1>Service Catalog</h1>
<ul>
{{range $namespace, $mappings := .}}
  <h2>{{$namespace}}</h2>
  <ul>
{{range $mappings}}
  {{if .Annotations.serviceCatalogShow}}
    {{if eq .Annotations.serviceCatalogShow "true"}}
      <li>
        {{if .Annotations.serviceCatalogHref}}
          <a href="{{.Annotations.serviceCatalogHref}}">
        {{end}}
        {{.Annotations.serviceCatalogText}}
        {{if .Annotations.serviceCatalogHref}}
          </a>
        {{end}}
      </li>
    {{end}}
  {{end}}
{{end}}
  </ul>
{{end}}
</ul>
</body>
</html>