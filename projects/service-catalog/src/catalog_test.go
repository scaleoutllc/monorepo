package main

import (
	"encoding/json"
	"html/template"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"monorepo/shared/go/ambassador_mapping"
	"strconv"
	"testing"
)

const rrUrl = "https://www.youtube.com/watch?v=oHg5SJYRHA0"
const gUrl = "https://google.com"

var mockMappings = &ambassador_mapping.MappingList{
	Items: []ambassador_mapping.Mapping{
		mockMapping("wakawaka-ns", gUrl, "google", true),
		mockMapping("ns1", rrUrl, "service 1 hidden", false),
		mockMapping("ns1", rrUrl, "service 1 shown", true),
		mockMapping("ns2", "", "no url", true),
		mockMapping("ns2", "href", "text", true),
	},
}

const mockTmpl = `{{ range $namespace, $mappings := . }}{{$namespace}}|{{range $mappings}}{{if eq .Annotations.serviceCatalogShow "true"}}{{.Annotations.serviceCatalogHref}}{{.Annotations.serviceCatalogText}}{{end}}{{end}}{{end}}`

// note namespaces in alphabetical order NOT mapping order
var expectedCatalog = "ns1|" + rrUrl + "service 1 shownns2|no urlhreftextwakawaka-ns|" + gUrl + "google"

func mockMapping(namespace string, href string, text string, show bool) ambassador_mapping.Mapping {
	return ambassador_mapping.Mapping{
		ObjectMeta: metav1.ObjectMeta{
			Name:      href + text,
			Namespace: namespace,
			Annotations: map[string]string{
				"serviceCatalogShow": strconv.FormatBool(show),
				"serviceCatalogHref": href,
				"serviceCatalogText": text,
			},
		},
		Spec: json.RawMessage{},
	}
}

func TestRender(t *testing.T) {

	tmpl, tmplErr := template.New("main").Parse(mockTmpl)
	if tmplErr != nil {
		t.Fatal(tmplErr)
	}

	testCatalog := Catalog{
		mappings: mockMappings,
		tmpl:     tmpl,
	}

	result, err := testCatalog.render()
	if err != nil {
		t.Fatal(err)
	}

	if result != expectedCatalog {
		t.Fatalf("expected %s \n got %s", expectedCatalog, result)
	}

}
