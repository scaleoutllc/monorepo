package main

import (
	"bytes"
	"github.com/prometheus/common/log"
	"html/template"
	"monorepo/shared/go/ambassador_mapping"
	"net/http"
)

type Catalog struct {
	mappings *ambassador_mapping.MappingList
	tmpl     *template.Template
}

func (c *Catalog) render() (string, error) {
	namespacedMappings := make(map[string][]ambassador_mapping.Mapping)
	for _, mapping := range c.mappings.Items {
		namespacedMappings[mapping.Namespace] = append(namespacedMappings[mapping.Namespace], mapping)
	}
	var buf bytes.Buffer
	err := c.tmpl.Execute(&buf, namespacedMappings)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func CatalogHandler(namespace string, tmpl *template.Template) http.HandlerFunc {

	mappingClient := ambassador_mapping.GetRestClient()

	return func(res http.ResponseWriter, req *http.Request) {
		mappings, err := mappingClient.GetMappings(namespace)
		if err != nil {
			log.Fatal(err)
		}

		catalog := Catalog{
			mappings: &mappings,
			tmpl:     tmpl,
		}

		content, err := catalog.render()
		if err != nil {
			status := http.StatusInternalServerError
			res.WriteHeader(status)
			res.Write([]byte(http.StatusText(status)))
			log.Fatal(err)
		} else {
			res.Write([]byte(content))
		}
	}

}
