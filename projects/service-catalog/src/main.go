package main

import (
	"html/template"
	"monorepo/shared/go/loglevel"

	"log"
	"net/http"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"gopkg.in/tylerb/graceful.v1"

	"github.com/labstack/echo/v4/middleware"
	"monorepo/shared/go/endpoints"
	"monorepo/shared/go/version"
)

// bump
// This application lists services running inside the current namespace
func main() {
	log.Println("service catalog starting...")
	httpPort := os.Getenv("APP_SERVER_PORT")
	if httpPort == "" {
		log.Fatal("env APP_SERVER_PORT must be specified (e.g. 8080).")
	}

	// if located in a central or utility namespace show ALL mappings otherwise
	// only show ones from the namespace this is deployed into
	namespace := os.Getenv("APP_NAMESPACE")
	if namespace == "master" || namespace == "platform-main" || namespace == "main" {
		namespace = ""
	}

	tmpl, tmplErr := template.New("main").Parse(catalogTemplate)
	if tmplErr != nil {
		log.Fatal(tmplErr)
	}

	ready := true

	// initialize http server
	e := echo.New()
	e.Logger.SetLevel(loglevel.GetLogLevel())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// mount standard handlers
	e.GET("/version", echo.WrapHandler(endpoints.NewVersionHandler(version.Identifier())))

	// a kubernetes pod configured with a readiness probe on this endpoint will
	// not allow traffic on any ports unless this is returning a 200 status code
	e.GET("/readiness", echo.WrapHandler(endpoints.NewReadinessHandler(&ready)))

	// a kubernetes pod configured with a liveness probe will signal the scheduler
	// to restart this service after a specified number of failures.
	e.GET("/liveness", echo.WrapHandler(http.HandlerFunc(endpoints.LivenessHandler)))

	// mount service specific handlers
	e.GET("/", echo.WrapHandler(CatalogHandler(namespace, tmpl)))

	e.Server.Addr = ":" + httpPort

	e.Logger.Fatal(graceful.ListenAndServe(e.Server, 1*time.Minute))
}
