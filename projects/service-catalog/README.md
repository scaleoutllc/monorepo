# service-catalog
> get oriented

## What is this for?
This service provides a landing page for developer/operator facing environments
to help them get oriented in a project. It is designed to automatically detect
services in Kubernetes such that it can be deployed into any environment and
provide useful information with zero configuration.

Any kubernetes ingress annotated with `showOnServiceCatalog: "true"` will appear
at `/` served by this service.
