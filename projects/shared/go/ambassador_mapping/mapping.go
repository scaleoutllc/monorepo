package ambassador_mapping

import (
	"encoding/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const CrdName = "mappings"

// In the event we need to do more with the actual mapping spec we can expand the spec to actually be consumed
// using a new type definition below. By setting the spec to a json.RawMessage we avoid ever deserializing it
// this saves on resource consumption since right now we have no use for spec data
type Mapping struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              json.RawMessage `json:"spec"`
}

type MappingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Mapping `json:"items"`
}
