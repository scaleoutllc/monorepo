package ambassador_mapping

import "k8s.io/apimachinery/pkg/runtime"

// boiler plate @see: https://www.martin-helmich.de/en/blog/kubernetes-crd-client.html
func (in *Mapping) DeepCopyInto(out *Mapping) {
	out.TypeMeta = in.TypeMeta
	out.ObjectMeta = in.ObjectMeta
	out.Spec = in.Spec
}

func (in *Mapping) DeepCopyObject() runtime.Object {
	out := Mapping{}
	in.DeepCopyInto(&out)

	return &out
}

func (in *MappingList) DeepCopyObject() runtime.Object {
	out := MappingList{}
	out.TypeMeta = in.TypeMeta
	out.ListMeta = in.ListMeta

	if in.Items != nil {
		out.Items = make([]Mapping, len(in.Items))
		for i := range in.Items {
			in.Items[i].DeepCopyInto(&out.Items[i])
		}
	}

	return &out
}
