package ambassador_mapping

import (
	"context"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"log"
)

type MappingClient struct {
	K8sClient *rest.RESTClient
}

func GetRestClient() MappingClient {
	config, configErr := rest.InClusterConfig()
	if configErr != nil {
		log.Fatal(configErr)
	}

	schemeErr := AddToScheme(scheme.Scheme)
	if schemeErr != nil {
		log.Fatal(schemeErr)
	}

	crdConfig := *config
	crdConfig.ContentConfig.GroupVersion = &schema.GroupVersion{Group: GroupName, Version: GroupVersion}
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = serializer.WithoutConversionCodecFactory{CodecFactory: scheme.Codecs}
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	restClient, clientErr := rest.UnversionedRESTClientFor(&crdConfig)
	if clientErr != nil {
		log.Fatal(clientErr)
	}

	return MappingClient{
		K8sClient: restClient,
	}
}

func (c *MappingClient) GetMappings(namespace string) (MappingList, error) {
	mappings := MappingList{}
	ctx := context.TODO()
	err := c.K8sClient.
		Get().
		Resource(CrdName).
		Namespace(namespace).
		Do(ctx).
		Into(&mappings)

	return mappings, err
}
