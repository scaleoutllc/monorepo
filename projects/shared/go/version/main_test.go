package version

import "testing"

func TestCommit(t *testing.T) {
	expected := "unknown"
	if COMMIT != expected {
		t.Errorf("COMMIT: got %v wanted %v", COMMIT, expected)
	}
}

func TestIdentifier(t *testing.T) {
	expected := "commit: unknown"
	actual := Identifier()
	if actual != expected {
		t.Errorf("COMMIT: got %v wanted %v", actual, expected)
	}
}
