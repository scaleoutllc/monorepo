package version

import (
	"fmt"
)

// COMMIT should be overridden by a build flag.
var COMMIT = "unknown"

func Identifier() string {
	return fmt.Sprintf("commit: %s", COMMIT)
}
