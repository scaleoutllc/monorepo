# This file makes CI/CD aware of this project. If copying this, take care to
# find and replace EVERY reference to the existing project name. Comitting
# duplicate names will cause conflicts that break the entire CI/CD system.

# Commits to the main branch will cause a container to be published with this
# version. Existing versions will never be overwritten. This value does not
# control deployments. To deploy a new version of this project, modify the
# `deployment.yml` in the appropriate environment folder.
.service-news-version: v1.0.0 

# These values configure all jobs that run for this project.
.service-news:
  variables:
    PROJECT_NAME: service-news
    PROJECT_PATH: projects/service-news 
    TEST_COMMAND: make test
    BUILD_COMMAND: make build
    BUILD_IMAGE: $AZURE_REGISTRY/ci-build:main
    IDENTITY_NAME: azure-service-news-identity

.service-news-ci-triggers:
  # Changes to these files trigger test, build, publish and deploy stages.
  sources: &service-news-build-files
    - $PROJECT_PATH/.gitlab-ci.yml
    - $PROJECT_PATH/Dockerfile
    - $PROJECT_PATH/Makefile
    - $PROJECT_PATH/src/**/*   
    - $PROJECT_PATH/../shared/go/**/*
  # Changes to these files trigger deployments only.
  configs: &service-news-deploy-files
    - $PROJECT_PATH/environments/base/**/*
    - $PROJECT_PATH/environments/$ENVIRONMENT/**/*

###########################################################################
# Nothing below this line should be modified unless carefully considered. #
###########################################################################

# This job runs when commits land in the main branch that affect production
# configuration files. This controls what is deployed in production and how
# it is configured.
service-news-prod:  
  # See shared/gitlab for definitions of these job templates.
  extends: [.service-news, .cluster-azure-prod-eastus, .job-kubernetes]
  variables:
    # Used to find configuration in $PROJECT_PATH/environments/$ENVIRONMENT
    ENVIRONMENT: azure-prod-eastus
    NAMESPACE: main
    OMIT_TEST_STAGE: "true"
    OMIT_BUILD_STAGE: "true"
    OMIT_STOP_STAGE: "true"
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      changes: *service-news-deploy-files

# This job runs when commits land in the main branch. This controls testing,
# building, container publishing, and deployments to the canonical namespace
# in the cluster (main).
service-news-dev:
  extends: [.service-news, .cluster-azure-dev-eastus, .job-kubernetes]
  variables:
    # Used to find configuration in $PROJECT_PATH/environments/$ENVIRONMENT
    ENVIRONMENT: azure-dev-eastus
    NAMESPACE: main
    OMIT_STOP_STAGE: "true"
  rules:
    - if: $CI_COMMIT_BRANCH != "main"
      when: never
    # If files impacting a build for this project have been changed, test,
    # build and publish a new container tagged with: the current commit hash,
    # the namespace (main), and, if it doesn't already exist, the version at
    # the top of this file.
    - changes: *service-news-build-files
      variables:
        TAG_VERSION: "true"
        IMAGE_TAG: $CI_COMMIT_SHA # TODO: use `main` here?
    # If files impacting configuration have changed, re-deploy using the new
    # configuration.
    - changes: *service-news-deploy-files
      variables:
        IMAGE_TAG: main
        OMIT_TEST_STAGE: "true"
        OMIT_BUILD_STAGE: "true"

# This job runs when commits land in feature branches. This controls testing,
# building, container publishing and deployments to sandboxed "ephemeral"
# namespaces matching the branch name. These namespaces are deleted when the
# feature branch is deleted.
service-news-ephemeral:
  extends: [.service-news, .cluster-azure-dev-eastus, .job-kubernetes]
  variables:
    # Used to find configuration in $PROJECT_PATH/environments/$ENVIRONMENT
    ENVIRONMENT: ephemeral
    # https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    # The branch name, in lowercase, shortened to 63 bytes, and with
    # everything except 0-9 and a-z replaced with -. No leading / trailing
    # -. Use in URLs, host names and domain names.
    NAMESPACE: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    # If files impacting a build for this project have been changed, test,
    # build and publish a new container tagged with both the current commit
    # hash and the namespace. Then, deploy the container to the ephemeral
    # feature branch namespace.
    - if: $FIRST_PUSH_FOR_NEW_BRANCH
      changes: 
        paths: *service-news-build-files
        compare_to: refs/heads/main # https://gitlab.com/gitlab-org/gitlab/-/issues/389808
      variables:
        IMAGE_TAG: $CI_COMMIT_SHA
    - if: $FIRST_PUSH_FOR_NEW_BRANCH == null
      changes: *service-news-build-files
      variables:
        IMAGE_TAG: $CI_COMMIT_SHA
    # If files impacting configuration have changed, re-deploy the most
    # recent namespace tagged container using the new configuration.
    # TODO(tkellen):
    #   if no namespaced container exists for this branch yet, tag
    #   one using the latest from main, or just use main directly?
    - if: $FIRST_PUSH_FOR_NEW_BRANCH
      changes:
        paths: *service-news-deploy-files
        compare_to: refs/heads/main # https://gitlab.com/gitlab-org/gitlab/-/issues/389808
      variables:
        IMAGE_TAG: $NAMESPACE
        OMIT_TEST_STAGE: "true"
        OMIT_BUILD_STAGE: "true"
    - if: $FIRST_PUSH_FOR_NEW_BRANCH == null
      changes: *service-news-deploy-files
      variables:
        IMAGE_TAG: $NAMESPACE 
        OMIT_TEST_STAGE: "true"
        OMIT_BUILD_STAGE: "true"
    # If a wip file has been found, deploy the `main` tag, or the tag found in
    # the wip file.
    - exists: [$PROJECT_PATH/wip]
      variables:
        IMAGE_TAG: "main"
        IMAGE_TAG_IN_WIP_FILE: "true"
        OMIT_TEST_STAGE: "true"
        OMIT_BUILD_STAGE: "true"