package main

import (
	"context"
	"log"
	"math/rand"
	"monorepo/shared/go/loglevel"
	"net/http"
	"os"
	"time"

	"monorepo/shared/go/endpoints"
	"monorepo/shared/go/version"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gopkg.in/tylerb/graceful.v1"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/keyvault/azsecrets"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	log.Println("demo service starting...")

	// get required env variables to run
	httpPort := os.Getenv("APP_SERVER_PORT")
	if httpPort == "" {
		log.Fatal("env APP_SERVER_PORT must be specified (e.g. 8080).")
	}

	cred, azureErr := azidentity.NewDefaultAzureCredential(nil)

	mongoUri := os.Getenv("APP_MONGODB_URI")
	if mongoUri == "" {
		log.Println("env APP_MONGODB_URI not set, fetching from keyvault.")
		if azureErr != nil {
			log.Fatal(azureErr)
		}
		// if APP_MONGODB_URI is not provided by the environment, go get the
		// value from keyvault. This is code was written in 15 minutes and is
		// NOT an example of best practices. A shared library for purpose
		// should be used (or built).
		vaultURI := os.Getenv("AZURE_VAULT_URI")
		if vaultURI == "" {
			log.Fatal("env AZURE_VAULT_URI must be specified.")
		}
		client, err := azsecrets.NewClient(vaultURI, cred, nil)
		if err != nil {
			log.Fatalf("failed to find vault: %v", err)
		}
		resp, err := client.GetSecret(context.TODO(), "APP-MONGODB-URI", "", nil)
		if err != nil {
			log.Fatalf("failed to get the secret: %v", err)
		} else {
			log.Printf("APP-MONGODB-URI fetched from %s", vaultURI)
			mongoUri = *resp.Value
		}
	}

	databaseName := os.Getenv("APP_MONGODB_NAME")
	if databaseName == "" {
		log.Fatal("env APP_MONGODB_NAME must be specified.")
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoUri))
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	if err != nil {
		log.Fatal(err)
	}
	// for the purposes of demonstration leave this value as false for 5 + random
	// 1 seconds during startup. this value is used by a readiness probe below to
	// simulate startup time (when the service is running but not ready to serve
	// traffic). a more realistic use-case would be flagging this to true after a
	// cache has been warmed.
	ready := false
	go func() {
		time.Sleep((1 + time.Duration(rand.Intn(5))) * time.Second)
		ready = true
		log.Printf("demo service ready...")
	}()

	// initialize http server
	e := echo.New()
	e.Logger.SetLevel(loglevel.GetLogLevel())
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// mount standard handlers
	e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))
	e.GET("/version", echo.WrapHandler(endpoints.NewVersionHandler(version.Identifier())))

	// a kubernetes pod configured with a readiness probe on this endpoint will
	// not allow traffic on any ports unless this is returning a 200 status code
	e.GET("/readiness", echo.WrapHandler(endpoints.NewReadinessHandler(&ready)))

	// a kubernetes pod configured with a liveness probe will signal the scheduler
	// to restart this service after a specified number of failures.
	e.GET("/liveness", echo.WrapHandler(http.HandlerFunc(endpoints.LivenessHandler)))

	// mount service specific handlers
	e.Any("/", echo.WrapHandler(NewIndexHandler(client.Database(databaseName))))

	// create http server that will drain connections gracefully when shut down
	srv := &graceful.Server{
		Timeout: 10 * time.Second,
		Server: &http.Server{
			Handler:           e.Server.Handler,
			ReadTimeout:       1 * time.Minute,
			ReadHeaderTimeout: 1 * time.Minute,
			WriteTimeout:      10 * time.Second,
			Addr:              ":" + httpPort,
		},
	}
	e.Logger.Fatal(srv.ListenAndServe())
}
