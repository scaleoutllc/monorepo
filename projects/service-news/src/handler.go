package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	ops = promauto.NewCounter(prometheus.CounterOpts{
		Name: "processed_ops_total",
		Help: "The total number of processed events",
	})
)

func NewIndexHandler(db *mongo.Database) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		var results []bson.M
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		cur, err := db.Collection("demo").Find(ctx, bson.D{})
		if err != nil {
			log.Fatal(err)
		}
		if err = cur.All(context.TODO(), &results); err != nil {
			log.Fatal(err)
		}
		prettyJSON, _ := json.MarshalIndent(results, "", "  ")
		status := http.StatusOK
		res.WriteHeader(status)
		res.Write([]byte("hello world\n\n"))
		res.Write(prettyJSON)
		ops.Inc()
	}
}
