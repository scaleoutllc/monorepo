locals {
  envs = {
    unmasked = {
      AZURE_TENANT_ID = file("../.config/azure-tenant-id")
    }
  }
}

// TODO: convert to group variables so they cannot be viewed in the UI?
resource "gitlab_project_variable" "azure-unmasked" {
  for_each = local.envs.unmasked
  project  = gitlab_project.repo.id
  key      = each.key
  value    = each.value
  masked   = false
}