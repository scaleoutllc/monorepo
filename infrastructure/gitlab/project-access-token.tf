resource "gitlab_project_access_token" "ci" {
  project      = gitlab_project.repo.id
  name         = "ci-bot"
  access_level = "owner"
  scopes       = ["read_repository", "write_repository"]
}

resource "gitlab_project_variable" "ci" {
  project   = gitlab_project.repo.id
  key       = "PROJECT_ACCESS_TOKEN"
  value     = gitlab_project_access_token.ci.token
  protected = true
}