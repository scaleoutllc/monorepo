resource "gitlab_project" "repo" {
  name             = "monorepo"
  visibility_level = "public"
}

resource "gitlab_branch_protection" "main" {
  project                      = gitlab_project.repo.id
  branch                       = "main"
  code_owner_approval_required = true
}