data "terraform_remote_state" "azure-shared-dev-eastus-gitlab" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-gitlab"
  }
}

data "terraform_remote_state" "azure-shared-prod-eastus-gitlab" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-prod-eastus-gitlab"
  }
}

locals {
  azure-dev  = data.terraform_remote_state.azure-shared-dev-eastus-gitlab.outputs
  azure-prod = data.terraform_remote_state.azure-shared-prod-eastus-gitlab.outputs
}