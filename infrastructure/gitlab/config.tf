terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.10.0"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/gitlab"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/gitlab/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/gitlab/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}