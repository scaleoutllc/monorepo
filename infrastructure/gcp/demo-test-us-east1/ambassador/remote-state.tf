data "terraform_remote_state" "cluster" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "cluster"
  }
}

data "terraform_remote_state" "network" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "network"
  }
}

locals {
  cluster = data.terraform_remote_state.cluster.outputs
  network = data.terraform_remote_state.network.outputs
}