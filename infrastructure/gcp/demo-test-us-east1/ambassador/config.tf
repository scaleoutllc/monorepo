locals {
  config_path     = "${path.module}/../.config/"
  gcp_config_path = "${path.module}/../../.config/"
  area            = file("${local.config_path}/area")
  env             = file("${local.config_path}/env")
  region          = file("${local.config_path}/region")
  workspace       = basename(path.cwd)
  name            = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  project_id      = file("${local.gcp_config_path}/demo-project-id")
   domain          = "${file("${local.config_path}/subdomain")}.${file("${local.gcp_config_path}/domain")}"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "=4.68.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "=1.14.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "=2.18.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
}

provider "google" {
  project = local.project_id
}

data "google_client_config" "current" {}

data "google_compute_zones" "available" {
  region = local.region
  status = "UP"
}

provider "kubernetes" {
  host                   = local.cluster.endpoint
  cluster_ca_certificate = base64decode(local.cluster.ca_certificate)
  token                  = data.google_client_config.current.access_token
}

provider "kubectl" {
  host                   = local.cluster.endpoint
  cluster_ca_certificate = base64decode(local.cluster.ca_certificate)
  token                  = data.google_client_config.current.access_token
}

provider "helm" {
  kubernetes {
    host                   = local.cluster.endpoint
    cluster_ca_certificate = base64decode(local.cluster.ca_certificate)
    token                  = data.google_client_config.current.access_token
  }
}

terraform {
  backend "gcs" {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "ambassador"
  }
}