output "neg-name" {
  value = local.neg-http-name
}

output "neg-zone" {
  value = local.neg-zone
}

output "healthcheck-port" {
  value = local.healthcheck-port
}

output "http-port" {
  value = local.http-port
}