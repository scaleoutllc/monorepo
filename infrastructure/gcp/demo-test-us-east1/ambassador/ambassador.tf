locals {
  neg-http-name        = "${local.name}-http"
  neg-zone             = data.google_compute_zones.available.names.0
  healthcheck-port    = 8877
  http-port            = 8080
}

# This terraform configuration is equivalent to the following:
#
# helm repo add datawire https://app.getambassador.io
# helm repo update
# kubectl create namespace ambassador --dry-run=client -o yaml | kubectl apply -f -
# kubectl apply -f https://app.getambassador.io/yaml/edge-stack/3.4.0/aes-crds.yaml
# kubectl wait --timeout=90s --for=condition=available deployment emissary-apiext -n emissary-system
# helm upgrade --install ambassador --namespace ambassador datawire/edge-stack --values values.yml
#
# The installation of ambassador triggers the creation of gcp network
# endpoint groups which act as backends to a long lived load balancer
# created in the routing workspace.
data "http" "ambassador-preinstall" {
  url = "https://app.getambassador.io/yaml/edge-stack/3.4.0/aes-crds.yaml"
  request_headers = {
    Accept = "binary/octet-stream"
  }
}

data "kubectl_file_documents" "ambassador-crds" {
  content = data.http.ambassador-preinstall.response_body
}

resource "kubernetes_namespace_v1" "ambassador-namespaces" {
  for_each = toset(["ambassador", "emissary-system"])
  metadata {
    name = each.key
  }
}

resource "kubectl_manifest" "ambassador-crds" {
  for_each  = data.kubectl_file_documents.ambassador-crds.manifests
  yaml_body = each.value
  depends_on = [
    kubernetes_namespace_v1.ambassador-namespaces
  ]
}

// TODO: make multizoned, see
// https://wdenniss.com/autopilot-specific-zones
resource "helm_release" "ingress" {
  name       = "ambassador"
  namespace  = "ambassador"
  repository = "https://app.getambassador.io"
  chart      = "edge-stack"
  version    = "8.6.0"
  values = [<<EOF
emissary-ingress:
  ingressClassResource:
    enabled: false
  replicaCount: 1
  nodeSelector:
    topology.kubernetes.io/zone: "${local.neg-zone}"
  service:
    annotations:
      cloud.google.com/neg: '{"exposed_ports": {"8080":{"name": "${local.neg-http-name}"}}}'
    type: ClusterIP
    ports:
      - name: http
        port: ${local.http-port}
        targetPort: ${local.http-port}
      - name: healthcheck
        port: ${local.healthcheck-port}
        targetPort: ${local.healthcheck-port}
EOF
  ]
  depends_on = [
    google_compute_firewall.ingress-fix,
    kubectl_manifest.ambassador-crds
  ]
}

resource "kubectl_manifest" "ambassador-host" {
  yaml_body = <<EOF
apiVersion: getambassador.io/v3alpha1
kind: Host
metadata:
  name: k8s-dev-scaleout-online
  namespace: ambassador
spec:
  hostname: "*"
  # we don't have ambassador doing TLS termination, so we
  # need to explicitly allow routing of HTTP traffic
  acmeProvider:
    authority: none
  requestPolicy:
    insecure:
      action: Route
EOF
  depends_on = [
    helm_release.ingress
  ]
}

resource "kubectl_manifest" "ambassador-listener" {
  yaml_body = <<EOF
apiVersion: getambassador.io/v3alpha1
kind: Listener
metadata:
  name: ambassador
  namespace: ambassador
spec:
  port: ${local.http-port}
  protocol: HTTP
  hostBinding:
    namespace:
      from: ALL
  securityModel: XFP
EOF
  depends_on = [
    helm_release.ingress
  ]
}

// ref: https://stackoverflow.com/questions/70585521/clean-ambassador-edge-stack-install-on-gke-fails-with-dns-resolving/70608787#70608787
resource "google_compute_firewall" "ingress-fix" {
  name    = "${local.name}-emissary-apiext-webhook"
  network = local.network.id
  allow {
    protocol = "tcp"
    ports    = ["8443"]
  }
  source_ranges = [local.network.control-plane-network]
}