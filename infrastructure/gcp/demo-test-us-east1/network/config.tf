locals {
  config_path     = "${path.module}/../.config/"
  gcp_config_path = "${path.module}/../../.config/"
  area            = file("${local.config_path}/area")
  env             = file("${local.config_path}/env")
  region          = file("${local.config_path}/region")
  workspace       = basename(path.cwd)
  name            = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  project_id      = file("${local.gcp_config_path}/demo-project-id")
  // 10.10.0.0/20
  // 10.10.0.0/21 - pods (2046 hosts, smallest allowed is /21)		
  // 10.10.8.0/22 - services (1022 hosts)		
  // 10.10.12.0/23 - nodes (512 hosts)
  // 10.10.14.0/23 - private (512 hosts, for later use)
  network = {
    cidr = "10.10.12.0/23" // nodes
    ranges = {
      pods     = "10.10.0.0/21"
      services = "10.10.8.0/22"
      private  = "10.10.14.0/23"
    }
  }
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.68.0"
    }
  }
}

provider "google" {
  project = local.project_id
}

terraform {
  backend "gcs" {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "network"
  }
}