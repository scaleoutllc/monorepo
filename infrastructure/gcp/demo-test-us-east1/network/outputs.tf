output "id" {
  value = google_compute_network.main.id
}

output "subnet_id" {
  value = google_compute_subnetwork.main.id
}

output "control-plane-network" {
  value = "192.168.3.0/28"
}

