locals {
  config_path        = "${path.module}/../.config/"
  gcp_config_path    = "${path.module}/../../.config/"
  global_config_path = "${path.module}/../../../.config/"
  area               = file("${local.config_path}/area")
  env                = file("${local.config_path}/env")
  region             = file("${local.config_path}/region")
  workspace          = basename(path.cwd)
  name               = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  project_id         = file("${local.gcp_config_path}/demo-project-id")
  gitlab_project_id  = file("${local.global_config_path}/gitlab-project-id")
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "=4.68.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "=16.0.3"
    }
  }
}

provider "google" {
  project = local.project_id
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

terraform {
  backend "gcs" {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "cluster"
  }
}