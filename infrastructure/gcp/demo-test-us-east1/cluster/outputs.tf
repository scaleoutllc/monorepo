output "endpoint" {
  value = google_container_cluster.main.endpoint
}

output "ca_certificate" {
  value = google_container_cluster.main.master_auth.0.cluster_ca_certificate
}