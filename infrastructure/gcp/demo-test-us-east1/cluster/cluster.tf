resource "google_container_cluster" "main" {
  name               = local.name
  min_master_version = "1.25.8-gke.500"
  // Enable billing per pod and allow GKE to manage node pools and cluster upgrades.
  enable_autopilot         = true
  enable_l4_ilb_subsetting = true
  # Regional cluster, see:
  # https://cloud.google.com/kubernetes-engine/docs/concepts/types-of-clusters
  location = local.region
  # Run in our own VPC, see:
  # https://cloud.google.com/kubernetes-engine/docs/concepts/private-cluster-concept
  networking_mode = "VPC_NATIVE"
  network         = local.network.id
  subnetwork      = local.network.subnet_id
  ip_allocation_policy {
    // Refers to secondary_ip_range entries in subnetwork.
    cluster_secondary_range_name  = "pods"
    services_secondary_range_name = "services"
  }
  private_cluster_config {
    enable_private_nodes   = true
    master_ipv4_cidr_block = local.network.control-plane-network
  }
  # List of networks that can contact the control plane.
  master_authorized_networks_config {
    cidr_blocks {
      cidr_block   = "0.0.0.0/0"
      display_name = "all-for-testing"
    }
  }
}

resource "google_service_account" "main" {
  account_id = local.name
  project    = local.project_id
}

resource "google_project_iam_member" "cluster-log-writer" {
  project = local.project_id
  role    = "roles/logging.logWriter"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "cluster-metric-writer" {
  project = local.project_id
  role    = "roles/monitoring.metricWriter"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "cluster-monitoring-viewer" {
  project = local.project_id
  role    = "roles/monitoring.viewer"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "cluster-resource-metadata-writer" {
  project = local.project_id
  role    = "roles/stackdriver.resourceMetadata.writer"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "cluster-gcr" {
  project = local.project_id
  role    = "roles/storage.objectViewer"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "cluster-artifact-registry" {
  project = local.project_id
  role    = "roles/artifactregistry.reader"
  member  = google_service_account.main.member
  //TODO: scope to specific registry?
}