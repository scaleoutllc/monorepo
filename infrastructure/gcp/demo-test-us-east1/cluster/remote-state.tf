data "terraform_remote_state" "network" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "network"
  }
}

data "terraform_remote_state" "dns" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "dns"
  }
}

locals {
  network = data.terraform_remote_state.network.outputs
  dns     = data.terraform_remote_state.dns.outputs
}