resource "google_project_service" "project" {
  for_each = toset([
    "artifactregistry.googleapis.com",
    "compute.googleapis.com",
    "networkmanagement.googleapis.com",
    "certificatemanager.googleapis.com",
    "container.googleapis.com",
    "cloudresourcemanager.googleapis.com"
  ])
  project            = local.project_id
  service            = each.key
  disable_on_destroy = true
}