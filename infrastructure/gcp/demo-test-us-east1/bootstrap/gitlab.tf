resource "google_service_account" "main" {
  account_id = local.name
}

resource "google_service_account_key" "main" {
  service_account_id = google_service_account.main.name
}

resource "google_project_iam_member" "owner" {
  project = local.project_id
  role    = "roles/owner"
  member  = google_service_account.main.member
}

resource "google_project_iam_member" "container-admin" {
  project = local.project_id
  role    = "roles/container.admin"
  member  = google_service_account.main.member
}

resource "gitlab_project_variable" "credentials" {
  project = local.gitlab_project_id
  key     = "GCP_${upper(replace(local.name, "-", "_"))}_AUTH"
  value   = google_service_account_key.main.private_key
  masked  = true
}