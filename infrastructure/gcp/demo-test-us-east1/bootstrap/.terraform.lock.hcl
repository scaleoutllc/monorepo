# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.0.3"
  constraints = "16.0.3"
  hashes = [
    "h1:NBCRbq9yjFnIifKqWzcfyYsUNH454Mgn4IrIIK7VJbA=",
    "zh:2e94e62242b92bd50493934523f522ba48fa2454fe8c8044e8bebd1c243ee655",
    "zh:3f47e1e92da6be38f6ddcfe23d24c41967c8374df4bd925ae45252b915f35a98",
    "zh:4e69de8d2bff4afde7f84f6be9fbc72f6b26831ea9aaba840618dba879ffd4fc",
    "zh:5c891d3c164cd45344faddefa0cc37211de5348f07fa766bd53cbb85cc6f55e9",
    "zh:747b3603bae67345990b49ed40c72e18e68e5459bb9a6d56f49c142e036503d0",
    "zh:872324d8c7471ca8f7b9da67a5016c5d22403f275cbba2c101e199d74d242196",
    "zh:8afee76e0dd0c3a17301c8d9482390b4daef57a35d2ab484dfb57bdb77d9b66b",
    "zh:8b55c6509d7fabeabba33e392a65302dbdc0583e4d0b32278a6f21efbc3cabbb",
    "zh:b0c48767a25a78c451587a7dd06171e58eb5a4086ecfd5c2d797a3c20989b6e8",
    "zh:c4a982d6e5198442a2f5d08c2c9c6273c78bc4fd2eda094b8ad3199c42308843",
    "zh:c71fdf664e564e5b7c0b6fe3bd93684befe162e0de2cb5a5e85bf92f59519be6",
    "zh:dc566e88a74cc5ae2cdaabb871b852a200c40edf77985e3a32d83efc4b2dfd21",
    "zh:f5277be064e7417bcbfe238459620648ea055ebae1d024957e3e92a37e772ea6",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:fd7c977b6711e7a242b3454f9afe1ab32c402d3440a460ea35a4fa7c0c8b6e9c",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "4.68.0"
  constraints = "4.68.0"
  hashes = [
    "h1:sYXf5T0qaB+Tz4fAEdniILZx6cGhshgiem/ZNkWve/8=",
    "zh:094f4d7b5816c68a8b948fd363a435da4bc47ba3cc60fa5e6676dec296aaaf3e",
    "zh:26a7fa7792eda30c8a411d71eeffa0cb4af45cb8d5eb6977fc01d880546c2ee7",
    "zh:331270e22dd10911ba599803592203bc7b4dff7c40489a8ce8875e1a940de808",
    "zh:4345c4359689db03ca9641e674761525d28bc4c01fe541f7e1e76af0e210cda3",
    "zh:5208f5be9f2cdc60136566700630242462d38bb8abfa17ba4e25645dd42a0c19",
    "zh:85aa3fa50a23842dde0144a39f7329656c8bb3a526aa9744ef1da88e0e284f2e",
    "zh:b89ab408e1ad5932be1e9cfc5d4e554d067050fcc5903a9e268d4e19fb35677e",
    "zh:c331adc2837e4f05047cdf79a36d48468e689658a7deae1aa3bbddfdee55e458",
    "zh:ca121bcb1d640f8f8bec243227e9958bd78048ef0da5850618f542aa1ef0745b",
    "zh:df352506473acdaddb4df120862ae144f238a93a2727511751d8a5baba88f7b2",
    "zh:e48460d2a0fff75046255934026a7da6bdb058677feed3107105278d0baa032a",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}
