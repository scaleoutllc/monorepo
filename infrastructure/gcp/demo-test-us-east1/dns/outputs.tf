output "zone_name" {
  value = google_dns_managed_zone.main.name
}

output "name_servers" {
  value = google_dns_managed_zone.main.name_servers
}

output "certificate_map_id" {
  value = google_certificate_manager_certificate_map.main.id
}