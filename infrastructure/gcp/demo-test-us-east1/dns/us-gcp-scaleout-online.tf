resource "google_dns_managed_zone" "main" {
  name     = replace(local.domain, ".", "-")
  dns_name = "${local.domain}."
  dnssec_config {
    state = "on"
  }
}

resource "google_certificate_manager_dns_authorization" "main" {
  name   = replace(local.domain, ".", "-")
  domain = local.domain
}

resource "google_dns_record_set" "authorize" {
  managed_zone = google_dns_managed_zone.main.name
  name         = google_certificate_manager_dns_authorization.main.dns_resource_record[0].name
  type         = google_certificate_manager_dns_authorization.main.dns_resource_record[0].type
  rrdatas      = [google_certificate_manager_dns_authorization.main.dns_resource_record[0].data]
}

resource "google_certificate_manager_certificate" "main" {
  name = replace(local.domain, ".", "-")
  managed {
    domains = [
      local.domain,
      "*.${local.domain}"
    ]
    dns_authorizations = [
      google_certificate_manager_dns_authorization.main.id
    ]
  }
}

resource "google_certificate_manager_certificate_map" "main" {
  name = replace(local.domain, ".", "-")
}

resource "google_certificate_manager_certificate_map_entry" "wildcard" {
  name         = "wildcard-${replace(local.domain, ".", "-")}"
  map          = google_certificate_manager_certificate_map.main.name
  certificates = [google_certificate_manager_certificate.main.id]
  hostname     = "*.${local.domain}"
}

resource "google_certificate_manager_certificate_map_entry" "apex" {
  name         = "apex-${replace(local.domain, ".", "-")}"
  map          = google_certificate_manager_certificate_map.main.name
  certificates = [google_certificate_manager_certificate.main.id]
  hostname     = local.domain
}