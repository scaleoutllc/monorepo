locals {
  config_path     = "${path.module}/../.config/"
  gcp_config_path = "${path.module}/../../.config/"
  area            = file("${local.config_path}/area")
  env             = file("${local.config_path}/env")
  region          = file("${local.config_path}/region")
  workspace       = basename(path.cwd)
  name            = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  project_id      = file("${local.gcp_config_path}/demo-project-id")
  domain          = "${file("${local.config_path}/subdomain")}.${file("${local.gcp_config_path}/domain")}"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "=4.68.0"
    }

  }
}

provider "google" {
  project = local.project_id
}

data "google_client_config" "current" {}

data "google_compute_zones" "available" {
  region = local.region
  status = "UP"
}

terraform {
  backend "gcs" {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "routing"
  }
}