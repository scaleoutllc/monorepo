// todo: rename ambassador to ambassador
data "terraform_remote_state" "ambassador" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "ambassador"
  }
}

data "terraform_remote_state" "dns" {
  backend = "gcs"
  config = {
    bucket = "demo-test-us-east1-tfstate"
    prefix = "dns"
  }
}


locals {
  ambassador = data.terraform_remote_state.ambassador.outputs
  dns        = data.terraform_remote_state.dns.outputs
}