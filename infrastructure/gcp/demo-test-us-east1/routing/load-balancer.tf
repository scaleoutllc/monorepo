data "google_compute_network_endpoint_group" "http" {
  name = local.ambassador.neg-name
  zone = local.ambassador.neg-zone
}

resource "google_compute_health_check" "ambassador" {
  name                = "${local.name}-ambassador"
  timeout_sec         = 1
  check_interval_sec  = 2
  healthy_threshold   = 1
  unhealthy_threshold = 3
  tcp_health_check {
    port = local.ambassador.healthcheck-port
  }
}

resource "google_compute_backend_service" "ingress" {
  name = "${local.name}-ingress"
  health_checks = [
    google_compute_health_check.ambassador.id
  ]
  protocol              = "HTTP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  backend {
    balancing_mode = "RATE"
    max_rate       = 1
    group          = data.google_compute_network_endpoint_group.http.self_link
  }
}

resource "google_compute_url_map" "ingress" {
  name            = "${local.name}-ingress"
  default_service = google_compute_backend_service.ingress.id
  host_rule {
    hosts = [
      local.domain,
      "*.${local.domain}"
    ]
    path_matcher = "all"
  }
  path_matcher {
    name            = "all"
    default_service = google_compute_backend_service.ingress.id
  }
}

resource "google_compute_target_https_proxy" "ingress" {
  name            = "${local.name}-tls-terminate"
  url_map         = google_compute_url_map.ingress.id
  certificate_map = "//certificatemanager.googleapis.com/${local.dns.certificate_map_id}"
}

resource "google_compute_global_forwarding_rule" "ingress" {
  name                  = "${local.name}-public"
  target                = google_compute_target_https_proxy.ingress.self_link
  port_range            = "443"
  load_balancing_scheme = "EXTERNAL_MANAGED"
}