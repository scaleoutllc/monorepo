resource "google_dns_record_set" "wildcard" {
  managed_zone = local.dns.zone_name
  name         = "*.${local.domain}."
  type         = "A"
  rrdatas      = [google_compute_global_forwarding_rule.ingress.ip_address]
}

resource "google_dns_record_set" "apex" {
  managed_zone = local.dns.zone_name
  name         = "${local.domain}."
  type         = "A"
  rrdatas      = [google_compute_global_forwarding_rule.ingress.ip_address]
}