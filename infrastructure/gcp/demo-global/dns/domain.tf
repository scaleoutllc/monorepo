resource "google_dns_managed_zone" "main" {
  name     = replace(local.domain, ".", "-")
  dns_name = "${local.domain}."
  dnssec_config {
    state = "on"
  }
}