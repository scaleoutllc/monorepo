resource "google_dns_record_set" "delegate-us" {
  managed_zone = google_dns_managed_zone.main.name
  name         = "us.${local.domain}."
  type         = "NS"
  rrdatas = [
    # see outputs of demo-test-us-east-1/dns
    "ns-cloud-b1.googledomains.com.",
    "ns-cloud-b2.googledomains.com.",
    "ns-cloud-b3.googledomains.com.",
    "ns-cloud-b4.googledomains.com.",
  ]
}