locals {
  config_path        = "${path.module}/../.config/"
  global_config_path = "${path.module}/../../.config/"
  area               = file("${local.config_path}/area")
  workspace          = basename(path.cwd)
  region             = file("${local.config_path}/region")
  name               = "${local.area}-${local.region}-${local.workspace}"
  project_id         = file("${local.global_config_path}/demo-project-id")
  domain             = file("${local.global_config_path}/domain")
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.68.0"
    }
  }
}

provider "google" {
  project = local.project_id
}

terraform {
  backend "gcs" {
    bucket = "demo-global-tfstate"
    prefix = "dns"
  }
}