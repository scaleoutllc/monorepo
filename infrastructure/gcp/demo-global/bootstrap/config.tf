locals {
  project_id = file("${path.module}/../../.config/demo-project-id")
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "=4.68.0"
    }
  }
}

provider "google" {
  project = local.project_id
}

terraform {
  backend "gcs" {
    bucket = "demo-global-tfstate"
    prefix = "bootstrap"
  }
}

resource "google_storage_bucket" "state" {
  name          = "demo-global-tfstate"
  force_destroy = false
  location      = "US"
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
}