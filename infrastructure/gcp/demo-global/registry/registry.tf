resource "google_artifact_registry_repository" "main" {
  repository_id = "${local.name}-us"
  format        = "DOCKER"
  location      = "us"
}

// TODO: create registries in other worldwide regions?