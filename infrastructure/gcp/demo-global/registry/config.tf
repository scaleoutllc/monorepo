locals {
  config_path        = "${path.module}/../.config/"
  global_config_path = "${path.module}/../../.config/"
  area               = file("${local.config_path}/area")
  region             = file("${local.config_path}/region")
  workspace          = basename(path.cwd)
  name               = "${local.area}-${local.region}-${local.workspace}"
  project_id         = file("${local.global_config_path}/demo-project-id")
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "=4.68.0"
    }
  }
}

provider "google" {
  project = local.project_id
}

terraform {
  backend "gcs" {
    bucket = "demo-global-tfstate"
    prefix = "shared-sandbox-registry"
  }
}