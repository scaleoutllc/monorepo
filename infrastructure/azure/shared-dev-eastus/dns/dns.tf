resource "azurerm_dns_zone" "main" {
  name                = "dev.${file("../../.config/domain")}"
  resource_group_name = azurerm_resource_group.env.name
  tags                = local.tags
}