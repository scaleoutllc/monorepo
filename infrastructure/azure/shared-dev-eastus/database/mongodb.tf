resource "azurerm_cosmosdb_account" "mongodb" {
  name                          = "${local.name}-mongo"
  location                      = local.region
  resource_group_name           = azurerm_resource_group.env.name
  offer_type                    = "Standard"
  kind                          = "MongoDB"
  mongo_server_version          = "4.2"
  enable_automatic_failover     = false
  public_network_access_enabled = false
  backup {
    type = "Continuous"
  }
  capacity {
    total_throughput_limit = -1
  }
  geo_location {
    location          = local.region
    failover_priority = 0
    zone_redundant    = true
  }
  consistency_policy {
    consistency_level       = "Session"
    max_interval_in_seconds = 5
    max_staleness_prefix    = 100
  }
  capabilities {
    name = "EnableServerless"
  }
  capabilities {
    name = "DisableRateLimitingResponses"
  }
  capabilities {
    name = "EnableMongo"
  }
}

resource "azurerm_private_endpoint" "mongodb" {
  name                = "${local.name}-mongodb"
  location            = azurerm_resource_group.env.location
  resource_group_name = azurerm_resource_group.env.name
  subnet_id           = local.network.subnet_ids.services
  private_service_connection {
    name                           = "${local.name}-mongodb"
    subresource_names              = ["MongoDB"]
    is_manual_connection           = false
    private_connection_resource_id = azurerm_cosmosdb_account.mongodb.id
  }
  private_dns_zone_group {
    name                 = "${local.name}-mongodb"
    private_dns_zone_ids = [local.network.mongo_private_zone_id]
  }
}