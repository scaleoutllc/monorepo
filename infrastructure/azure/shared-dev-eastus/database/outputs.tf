output "mongo_account_name" {
  value = azurerm_cosmosdb_account.mongodb.name
}

output "resource_group_name" {
  value = azurerm_resource_group.env.name
}