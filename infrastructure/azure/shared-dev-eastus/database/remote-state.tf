data "terraform_remote_state" "azure-k8s-dev-eastus-network" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-network"
  }
}

data "terraform_remote_state" "azure-shared-dev-eastus-dns" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-dns"
  }
}

locals {
  network = data.terraform_remote_state.azure-k8s-dev-eastus-network.outputs
  dns     = data.terraform_remote_state.azure-shared-dev-eastus-dns.outputs
}