output "debug" {
  value     = <<EOF
export ARM_TENANT_ID="${data.azurerm_subscription.current.tenant_id}"
export ARM_SUBSCRIPTION_ID="${data.azurerm_subscription.current.tenant_id}"
export ARM_CLIENT_ID="${azuread_application.gitlab.application_id}"
export ARM_CLIENT_SECRET="${azuread_service_principal_password.gitlab.value}"
EOF
  sensitive = true
}
