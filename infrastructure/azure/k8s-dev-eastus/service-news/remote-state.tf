data "terraform_remote_state" "azure-shared-dev-eastus-database" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-database"
  }
}

data "terraform_remote_state" "azure-k8s-dev-eastus-cluster" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-cluster"
  }
}

locals {
  database = data.terraform_remote_state.azure-shared-dev-eastus-database.outputs
  cluster  = data.terraform_remote_state.azure-k8s-dev-eastus-cluster.outputs
}