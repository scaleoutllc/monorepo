resource "azurerm_cosmosdb_mongo_database" "news" {
  name                = local.warkspace
  resource_group_name = local.database.resource_group_name
  account_name        = local.database.mongo_account_name
}