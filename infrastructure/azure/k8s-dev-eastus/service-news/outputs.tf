output "vault_name" {
  value = azurerm_key_vault.main.name
}

output "secret_name" {
  value = kubernetes_secret_v1.identity.metadata[0].name
}

output "environment_manifest" {
  value = <<EOF
- name: AZURE_VAULT_URI
  valueFrom:
    secretKeyRef:
      name: ${azurerm_key_vault.main.vault_uri}
      key: AZURE_VAULT_URI
- name: AZURE_CLIENT_ID
  valueFrom:
    secretKeyRef:
      name: ${kubernetes_secret_v1.identity.metadata[0].name}
      key: AZURE_CLIENT_ID
- name: AZURE_CLIENT_SECRET
  valueFrom:
    secretKeyRef:
      name: ${kubernetes_secret_v1.identity.metadata[0].name}
      key: AZURE_CLIENT_SECRET
- name: AZURE_CLIENT_SECRET
  valueFrom:
    secretKeyRef:
      name: ${kubernetes_secret_v1.identity.metadata[0].name}
      key: AZURE_CLIENT_SECRET
- name: AZURE_TENANT_ID
  valueFrom:
    secretKeyRef:
      name: ${kubernetes_secret_v1.identity.metadata[0].name}
      key: AZURE_TENANT_ID
- name: AZURE_SUBSCRIPTION_ID
  valueFrom:
    secretKeyRef:
      name: ${kubernetes_secret_v1.identity.metadata[0].name}
      key: AZURE_SUBSCRIPTION_ID
EOF
}