resource "azurerm_key_vault" "main" {
  name                        = replace(local.name, "-", "")
  location                    = azurerm_resource_group.env.location
  resource_group_name         = azurerm_resource_group.env.name
  enabled_for_disk_encryption = true
  tenant_id                   = local.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false
  sku_name                    = "standard"
  enable_rbac_authorization   = true
}