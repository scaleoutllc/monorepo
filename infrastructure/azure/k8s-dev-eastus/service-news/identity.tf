# Each cloud provider has a solution for securely federating identities in their
# native IAM system to third party consumers (e.g. kubernetes pods). By following
# the principle of least privilege, these per-workload identities can securely
# grant minimal access to cloud provider resources (e.g. permission to read or
# write objects in storage account or retreive credentials from a keyvault).
#
# The Azure-native method for managing this capability can be found here:
# https://github.com/Azure/azure-workload-identity
#
# Unfortunately, the implementation is not yet suitable for real world usage.
# Federated identities in Azure can only issue 20 concurrent tokens per identity.
# When more than 20 instances of the same workload are running (very common in
# development on even the smallest of teams and quite common in production at
# any level of scale), tokens will cease to be issued and workloads will not
# be able to function.
# /ref https://github.com/Azure/azure-workload-identity/issues/575
#
# While it would be possible to work around these issues with clever automation,
# the complexity cost is deemed to be too high. Until these issues are resolved,
# workloads will receive their identity by placing service principal credentials
# in a per-application kubernetes secret. These creds can be used by any number
# of consumers. The resultant functionality, while slightly less secure, will
# be identical. If and when Azure resolves these issues we can migrate to the
# native solution with no impact to existing applications.
data "azurerm_client_config" "current" {}

resource "azuread_application" "main" {
  display_name = local.name
}

resource "azuread_application_password" "main" {
  application_object_id = azuread_application.main.object_id
}

resource "azuread_service_principal" "main" {
  application_id = azuread_application.main.application_id
  owners         = [data.azurerm_client_config.current.object_id]
}

resource "azurerm_role_assignment" "keyvault" {
  principal_id         = azuread_service_principal.main.id
  role_definition_name = "Key Vault Administrator"
  scope                = azurerm_key_vault.main.id
}

resource "kubernetes_secret_v1" "identity" {
  metadata {
    name      = "azure-service-news-identity"
    namespace = "kube-system"
  }
  type = "Opaque"
  data = {
    AZURE_CLIENT_ID       = azuread_application.main.application_id
    AZURE_CLIENT_SECRET   = azuread_application_password.main.value
    AZURE_TENANT_ID       = local.tenant_id
    AZURE_SUBSCRIPTION_ID = local.subscription_id
    AZURE_VAULT_URI       = azurerm_key_vault.main.vault_uri
  }
}

output "secret" {
  value = kubernetes_secret_v1.identity
  sensitive = true
}