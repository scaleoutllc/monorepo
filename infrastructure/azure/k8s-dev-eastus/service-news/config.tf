locals {
  area      = "k8s"
  env       = "dev"
  region    = "eastus"
  workspace = "service-news"
  name      = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-${local.area}-${local.env}-${local.region}/${local.workspace}"
  }
  tenant_id       = file("${path.module}/../../.config/azure-tenant-id")
  subscription_id = file("${path.module}/../../.config/azure-dev-subscription-id")
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.36.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "=2.18.1"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = local.subscription_id
}

provider "azuread" {}

provider "kubernetes" {
  username               = local.cluster.aks.kube_admin_config.0.username
  password               = local.cluster.aks.kube_admin_config.0.password
  host                   = local.cluster.aks.kube_admin_config.0.host
  client_certificate     = base64decode(local.cluster.aks.kube_admin_config.0.client_certificate)
  client_key             = base64decode(local.cluster.aks.kube_admin_config.0.client_key)
  cluster_ca_certificate = base64decode(local.cluster.aks.kube_admin_config.0.cluster_ca_certificate)
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-service-news"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-service-news/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-service-news/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

resource "azurerm_resource_group" "env" {
  name     = local.name
  location = local.region
  tags     = local.tags
}