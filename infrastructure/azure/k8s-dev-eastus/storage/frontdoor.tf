resource "azurerm_private_endpoint" "main" {
  name                = "${local.name}-ingress"
  location            = azurerm_resource_group.env.location
  resource_group_name = azurerm_resource_group.env.name
  subnet_id           = local.network.subnet_ids.services
  private_service_connection {
    name                           = "${local.name}-web"
    subresource_names              = ["web"]
    is_manual_connection           = false
    private_connection_resource_id = azurerm_storage_account.main.id
  }
  private_dns_zone_group {
    name                 = "${local.name}-link"
    private_dns_zone_ids = [local.network.web_private_zone_id]
  }
}

resource "azurerm_cdn_frontdoor_origin_group" "ingress" {
  name                     = "${local.name}-ingress"
  cdn_frontdoor_profile_id = local.network.frontdoor_profile_id
  session_affinity_enabled = true
  load_balancing {}
}

resource "azurerm_cdn_frontdoor_origin" "ingress" {
  name                           = "${local.name}-ingress"
  cdn_frontdoor_origin_group_id  = azurerm_cdn_frontdoor_origin_group.ingress.id
  enabled                        = true
  host_name                      = azurerm_storage_account.main.primary_web_host
  origin_host_header             = azurerm_storage_account.main.primary_web_host
  priority                       = 1 // lowest comes first
  weight                         = 1000
  certificate_name_check_enabled = true
  private_link {
    target_type            = "web"
    location               = azurerm_resource_group.env.location
    private_link_target_id = azurerm_storage_account.main.id
  }
}

resource "azurerm_cdn_frontdoor_route" "main" {
  name                            = "${local.name}-ingress"
  cdn_frontdoor_endpoint_id       = local.network.frontdoor_endpoint_id
  cdn_frontdoor_origin_group_id   = azurerm_cdn_frontdoor_origin_group.ingress.id
  cdn_frontdoor_origin_ids        = [azurerm_cdn_frontdoor_origin.ingress.id]
  enabled                         = true
  https_redirect_enabled          = true
  forwarding_protocol             = "HttpsOnly"
  patterns_to_match               = ["","/","/index.html","/static/*"]
  supported_protocols             = ["Http", "Https"]
  cdn_frontdoor_custom_domain_ids = [
    local.network.frontdoor_custom_domain_id_cluster,
    local.network.frontdoor_custom_domain_id_cluster_wildcard
  ]
  link_to_default_domain          = false
}