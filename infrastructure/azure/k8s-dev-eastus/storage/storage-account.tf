resource "azurerm_storage_account" "main" {
  name                            = replace("${local.area}${local.env}${local.region}storage", "-", "")
  resource_group_name             = azurerm_resource_group.env.name
  location                        = local.region
  account_tier                    = "Standard"
  account_replication_type        = "LRS"
  account_kind                    = "StorageV2"
  public_network_access_enabled   = false
  allow_nested_items_to_be_public = false
  static_website {
    index_document     = "index.html"
    error_404_document = "index.html"
  }
  tags = local.tags
}

resource "azurerm_storage_account_network_rules" "main" {
  storage_account_id = azurerm_storage_account.main.id
  default_action     = "Deny"
}