locals {
  area      = "k8s"
  env       = "dev"
  region    = "eastus"
  workspace = "storage"
  name      = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-${local.area}-${local.env}-${local.region}/${local.workspace}"
  }
  tenant_id         = file("${path.module}/../../.config/azure-tenant-id")
  subscription_id   = file("${path.module}/../../.config/azure-dev-subscription-id")
  gitlab_project_id = file("${path.module}/../../../.config/gitlab-project-id")
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = local.subscription_id
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-storage"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-storage/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-storage/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

resource "azurerm_resource_group" "env" {
  name     = local.name
  location = local.region
  tags     = local.tags
}