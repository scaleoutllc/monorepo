data "terraform_remote_state" "azure-k8s-dev-eastus-network" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/${local.gitlab_project_id}/terraform/state/azure-k8s-dev-eastus-network"
  }
}

locals {
  network = data.terraform_remote_state.azure-k8s-dev-eastus-network.outputs
}