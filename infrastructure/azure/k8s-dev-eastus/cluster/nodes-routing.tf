resource "azurerm_kubernetes_cluster_node_pool" "routing" {
  kubernetes_cluster_id = azurerm_kubernetes_cluster.main.id
  name                  = "routing"
  vm_size               = "Standard_B2s"
  node_count            = 1
  vnet_subnet_id        = local.network.subnet_ids.routing
  zones                 = [1, 2, 3]
  max_pods              = 10
  node_labels = {
    "node.kubernetes.io/routing" = "true"
  }
  node_taints = [
    "node.kubernetes.io/routing=true:NoSchedule"
  ]
  tags = local.tags
}