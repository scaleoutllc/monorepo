resource "azurerm_kubernetes_cluster_node_pool" "app" {
  kubernetes_cluster_id = azurerm_kubernetes_cluster.main.id
  name                  = "app"
  vm_size               = "Standard_B2s"
  node_count            = 1
  vnet_subnet_id        = local.network.subnet_ids.apps2
  zones                 = [1, 2, 3]
  max_pods              = 250
  node_labels = {
    "node.kubernetes.io/app" = "true"
  }
  tags = local.tags
}
