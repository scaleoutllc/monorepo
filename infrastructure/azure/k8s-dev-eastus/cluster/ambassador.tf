# This terraform configuration is equivalent to the following:
#
# helm repo add datawire https://app.getambassador.io
# helm repo update
# kubectl create namespace ambassador --dry-run=client -o yaml | kubectl apply -f -
# kubectl apply -f https://app.getambassador.io/yaml/edge-stack/3.4.0/aes-crds.yaml
# kubectl wait --timeout=90s --for=condition=available deployment emissary-apiext -n emissary-system
# helm upgrade --install ambassador --namespace ambassador datawire/edge-stack --values values.yml
#
# The installation of ambassador triggers the creation of an azure load balancer
# (kubernetes-internal) that acts as a single point of entry for network traffic
# into the cluster. This load balancer must exist before a private link to it can
# be created. The private link must exist before it can be used as a secure origin
# for frontdoor (which provides TLS termination for network traffic arriving from
# outside the cluster). This method of installation enables creating a cluster ready
# to receive traffic in a single step. Without this approach it would be neccesary
# to create the cluster in one step, install ambassador in another and configure
# a privatelink and front door in yet another.
data "http" "ambassador-preinstall" {
  url = "https://app.getambassador.io/yaml/edge-stack/3.4.0/aes-crds.yaml"
  request_headers = {
    Accept = "binary/octet-stream"
  }
}

data "kubectl_file_documents" "ambassador-crds" {
  content = data.http.ambassador-preinstall.response_body
}

resource "kubernetes_namespace_v1" "ambassador-namespaces" {
  for_each = toset(["ambassador", "emissary-system"])
  metadata {
    name = each.key
  }
  depends_on = [
    azurerm_kubernetes_cluster.main
  ]
}

resource "kubectl_manifest" "ambassador-crds" {
  for_each  = data.kubectl_file_documents.ambassador-crds.manifests
  yaml_body = each.value
  depends_on = [
    kubernetes_namespace_v1.ambassador-namespaces
  ]
}

resource "helm_release" "ingress" {
  name       = "ambassador"
  namespace  = "ambassador"
  repository = "https://app.getambassador.io"
  chart      = "edge-stack"
  version    = "8.6.0"
  values = [<<EOF
emissary-ingress:
  ingressClassResource:
    enabled: false
  daemonSet: true
  nodeSelector:
    node.kubernetes.io/routing: "true"
  tolerations:
    - key: node.kubernetes.io/routing
      operator: Equal
      value: "true"
      effect: NoSchedule
  service:
    annotations:
      service.beta.kubernetes.io/azure-load-balancer-internal: "true"
      service.beta.kubernetes.io/azure-load-balancer-internal-subnet: "routing"
    type: LoadBalancer
    externalTrafficPolicy: Local
    ports:
      - name: http
        port: 8080
        targetPort: 8080
      - name: healthcheck
        port: 80
        targetPort: 8877
  agent:
    nodeSelector:
      node.kubernetes.io/routing: "true"
    tolerations:
      - key: node.kubernetes.io/routing
        operator: Equal
        value: "true"
        effect: NoSchedule
redis:
  nodeSelector:
    node.kubernetes.io/routing: "true"
  tolerations:
    - key: node.kubernetes.io/routing
      operator: Equal
      value: "true"
      effect: NoSchedule
EOF
  ]
  depends_on = [
    kubectl_manifest.ambassador-crds
  ]
}

resource "kubectl_manifest" "ambassador-host" {
  yaml_body = <<EOF
apiVersion: getambassador.io/v3alpha1
kind: Host
metadata:
  name: k8s-dev-scaleout-online
  namespace: ambassador
spec:
  hostname: "*"
  # we don't have ambassador doing TLS termination, the frontdoor does that.
  # so we need to explicitly allow routing of HTTP traffic from frontdoor
  acmeProvider:
    authority: none
  requestPolicy:
    insecure:
      action: Route
EOF
  depends_on = [
    helm_release.ingress
  ]
}

resource "kubectl_manifest" "ambassador-listener" {
  yaml_body = <<EOF
apiVersion: getambassador.io/v3alpha1
kind: Listener
metadata:
  name: ambassador
  namespace: ambassador
spec:
  port: 8080
  protocol: HTTP
  hostBinding:
    namespace:
      from: ALL
  securityModel: XFP
EOF
  depends_on = [
    helm_release.ingress
  ]
}