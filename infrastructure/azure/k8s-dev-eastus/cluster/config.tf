locals {
  area      = "k8s"
  env       = "dev"
  region    = "eastus"
  workspace = "cluster"
  name      = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-${local.area}-${local.env}-${local.region}/${local.workspace}"
  }
  tenant_id         = file("${path.module}/../../.config/azure-tenant-id")
  subscription_id   = file("${path.module}/../../.config/azure-dev-subscription-id")
  gitlab_project_id = file("${path.module}/../../../.config/gitlab-project-id")
}

terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.37.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "=2.18.1"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
  }
}

provider "azuread" {}

provider "azurerm" {
  features {}
  subscription_id = local.subscription_id
}

provider "kubernetes" {
  username               = azurerm_kubernetes_cluster.main.kube_admin_config.0.username
  password               = azurerm_kubernetes_cluster.main.kube_admin_config.0.password
  host                   = azurerm_kubernetes_cluster.main.kube_admin_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.cluster_ca_certificate)
}

provider "kubectl" {
  username               = azurerm_kubernetes_cluster.main.kube_admin_config.0.username
  password               = azurerm_kubernetes_cluster.main.kube_admin_config.0.password
  host                   = azurerm_kubernetes_cluster.main.kube_admin_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    username               = azurerm_kubernetes_cluster.main.kube_admin_config.0.username
    password               = azurerm_kubernetes_cluster.main.kube_admin_config.0.password
    host                   = azurerm_kubernetes_cluster.main.kube_admin_config.0.host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.main.kube_admin_config.0.cluster_ca_certificate)
  }
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-cluster"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-cluster/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-cluster/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

resource "azurerm_resource_group" "env" {
  name     = local.name
  location = local.region
  tags     = local.tags
}