data "terraform_remote_state" "azure-k8s-dev-eastus-network" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/${local.gitlab_project_id}/terraform/state/azure-k8s-dev-eastus-network"
  }
}

data "terraform_remote_state" "azure-users" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/${local.gitlab_project_id}/terraform/state/azure-users"
  }
}

data "terraform_remote_state" "azure-shared-dev-eastus-dns" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/${local.gitlab_project_id}/terraform/state/azure-shared-dev-eastus-dns"
  }
}

data "terraform_remote_state" "azure-shared-dev-eastus-registry" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/${local.gitlab_project_id}/terraform/state/azure-shared-prod-eastus-registry"
  }
}

locals {
  network  = data.terraform_remote_state.azure-k8s-dev-eastus-network.outputs
  users    = data.terraform_remote_state.azure-users.outputs
  dns      = data.terraform_remote_state.azure-shared-dev-eastus-dns.outputs
  registry = data.terraform_remote_state.azure-shared-dev-eastus-registry.outputs
}