resource "azurerm_role_assignment" "registry-access" {
  principal_id                     = azurerm_kubernetes_cluster.main.kubelet_identity[0].object_id
  role_definition_name             = "AcrPull"
  scope                            = local.registry.id
  skip_service_principal_aad_check = true
}