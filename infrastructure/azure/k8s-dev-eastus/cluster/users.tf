resource "azuread_group_member" "admin" {
  for_each = toset([
    "root"
  ])
  group_object_id  = azuread_group.admin.id
  member_object_id = local.users.map[each.key].id
}
