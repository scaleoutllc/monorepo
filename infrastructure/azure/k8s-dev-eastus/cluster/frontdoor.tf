// locate load balancer created by aks
data "azurerm_lb" "ingress" {
  name                = "kubernetes-internal"
  resource_group_name = azurerm_kubernetes_cluster.main.node_resource_group
  depends_on = [
    helm_release.ingress
  ]
}

// calculate IP of private link service that hits kubernetes-internal load balancer
locals {
  hosts      = range(pow(2, 32 - split("/", local.network.subnets.routing)[1]))
  usable_ips = toset(slice(local.hosts, 4, length(local.hosts) - 1)) // https://learn.microsoft.com/en-us/azure/virtual-network/virtual-networks-faq#are-there-any-restrictions-on-using-ip-addresses-within-these-subnets
  ingress_ip = cidrhost(local.network.subnets.routing, length(local.usable_ips))
}

resource "azurerm_private_link_service" "ingress" {
  name                = "${azurerm_kubernetes_cluster.main.name}-ingress"
  resource_group_name = azurerm_kubernetes_cluster.main.node_resource_group
  location            = local.region

  auto_approval_subscription_ids = [local.subscription_id]
  visibility_subscription_ids    = ["*"]
  load_balancer_frontend_ip_configuration_ids = [
    data.azurerm_lb.ingress.frontend_ip_configuration.0.id
  ]
  nat_ip_configuration {
    name               = "primary"
    private_ip_address = local.ingress_ip
    subnet_id          = local.network.subnet_ids.routing
    primary            = true
  }
  depends_on = [
    helm_release.ingress
  ]
}

resource "azurerm_cdn_frontdoor_origin_group" "ingress" {
  name                     = "${azurerm_kubernetes_cluster.main.name}-ingress"
  cdn_frontdoor_profile_id = local.network.frontdoor_profile_id
  session_affinity_enabled = false
  // ports are not configurable for this health probe. this is polling port 80
  // on the kubernetes-internal load balancer which is created by ambassador
  // in helm_release.ingress.
  health_probe {
    protocol            = "Http"
    request_type        = "GET"
    path                = "/ambassador/v0/check_ready"
    interval_in_seconds = 30
  }
  load_balancing {}
}

resource "azurerm_cdn_frontdoor_origin" "ingress" {
  name                          = "${azurerm_kubernetes_cluster.main.name}-ingress"
  cdn_frontdoor_origin_group_id = azurerm_cdn_frontdoor_origin_group.ingress.id
  enabled                       = true
  host_name                     = azurerm_private_link_service.ingress.nat_ip_configuration.0.private_ip_address
  priority                      = 2 // lowest comes first
  weight                        = 1000
  // this is hitting the kubernetes-internal load balancer which is created by
  // ambassador in helm_release.ingress
  http_port                      = 8080
  certificate_name_check_enabled = true
  private_link {
    location               = local.region
    private_link_target_id = azurerm_private_link_service.ingress.id
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "azurerm_cdn_frontdoor_route" "main" {
  name                          = "${azurerm_kubernetes_cluster.main.name}-ingress"
  cdn_frontdoor_endpoint_id     = local.network.frontdoor_endpoint_id
  cdn_frontdoor_origin_group_id = azurerm_cdn_frontdoor_origin_group.ingress.id
  cdn_frontdoor_origin_ids      = [azurerm_cdn_frontdoor_origin.ingress.id]
  enabled                       = true
  https_redirect_enabled        = true
  forwarding_protocol           = "HttpOnly"
  patterns_to_match             = ["/*"]
  supported_protocols           = ["Http", "Https"]
  cdn_frontdoor_custom_domain_ids = [
    local.network.frontdoor_custom_domain_id_cluster,
    local.network.frontdoor_custom_domain_id_cluster_wildcard
  ]
  link_to_default_domain = false
}