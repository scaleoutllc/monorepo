output "aks" {
  value     = azurerm_kubernetes_cluster.main
  sensitive = true
}

output "resource_group_name" {
  value = local.name
}