resource "azurerm_kubernetes_cluster" "main" {
  name                = local.name
  kubernetes_version  = "1.25"
  location            = azurerm_resource_group.env.location
  resource_group_name = azurerm_resource_group.env.name
  dns_prefix          = local.area
  network_profile {
    network_plugin = "azure"
    network_policy = "azure"
  }
  tags = local.tags

  // Make it possible for pods to assume azure identities.
  // Requires one time run of the following:
  //   az feature register --namespace "Microsoft.ContainerService" --name "EnableWorkloadIdentityPreview"
  //   -- wait ~10 minutes for feature to be enabled, check for "Registered" status with:
  //   az feature show --namespace "Microsoft.ContainerService" --name "EnableWorkloadIdentityPreview"
  //   -- apply with
  //   az provider register --namespace Microsoft.ContainerService
  // workload_identity_enabled = true
  // ^ as of 2023-03-01 this is entirely unbaked and runs v0.15.0 of https://github.com/Azure/azure-workload-identity
  // uncomment if/when this is actually handled--for now this is managed manually via the finish state.
  oidc_issuer_enabled = true

  // Disable auth using native k8s accounts.
  // local_account_disabled = true
  // ^ if uncommented, this (untested) method of auth is needed for kubernetes provider:
  //
  //   provider "kubernetes" {
  //     host                   = local.cluster.kube_config.0.host
  //     cluster_ca_certificate = base64decode(local.cluster.kube_config.0.cluster_ca_certificate)
  //     exec {
  //       api_version = "client.authentication.k8s.io/v1beta1"
  //       command = "./kubelogin"
  //       args = [
  //         "get-token",
  //         "--login",
  //         "spn",
  //         "--environment",
  //         "AzurePublicCloud",
  //         "--tenant-id",
  //         var.tenant_id,
  //         "--server-id",
  //         var.aad_server_id,
  //         "--client-id",
  //         var.client_id,
  //         "--client-secret",
  //         var.client_secret
  //       ]
  //     }
  //   }

  // Enable azure ad role based authentication for cluster access.
  azure_active_directory_role_based_access_control {
    managed                = true
    admin_group_object_ids = [azuread_group.admin.id]
    azure_rbac_enabled     = true
  }

  // Make node communicate with control plane over private vnet.
  // Requires one time run of the following:
  //   az extension add --name aks-preview
  //   az extension update --name aks-preview
  //   az feature register --namespace "Microsoft.ContainerService" --name "EnableAPIServerVnetIntegrationPreview"
  //   -- wait ~10 minutes for feature to be enabled, check for "Registered" status with:
  //   az feature show --namespace "Microsoft.ContainerService" --name "EnableAPIServerVnetIntegrationPreview"
  //   -- apply with:
  //   az provider register --namespace Microsoft.ContainerService
  // 
  // ref: https://learn.microsoft.com/en-us/azure/aks/api-server-vnet-integration#prerequisites
  api_server_access_profile {
    vnet_integration_enabled = true
    // use apiserver subnet, minted purely for this because node pools cannot exist alongside
    subnet_id = local.network.subnet_ids.apiserver
  }

  // As of 2022-03-01, user managed identity with explicit permissions is required when
  // using vnet_integration_enabled (the alternative is an azure managed identity that is
  // created and managed with the cluster automatically but does not yet have the proper
  // access).
  identity {
    type = "UserAssigned"
    identity_ids = [
      azurerm_user_assigned_identity.self.id
    ]
  }

  // Maintenance gotcha: updating this node pool will force recreation of the whole cluster.
  // ref: https://github.com/hashicorp/terraform-provider-azurerm/issues/7093
  default_node_pool {
    name           = "system"
    vm_size        = "Standard_B2s"
    node_count     = 1
    vnet_subnet_id = local.network.subnet_ids.apps1
    zones          = [1, 2, 3]
    node_labels = {
      "node.kubernetes.io/system" = "true"
    }
    // Indirect method to apply taint to prevent regular workloads from being scheduled
    // on these nodes. Assigning taints manually is not allowed for default_node_pool.
    only_critical_addons_enabled = true
    node_taints                  = [] // will be marked as changed if not set explicitly. 
    tags                         = local.tags
  }
  // Create a resource group for the default_node_group to reside in.
  node_resource_group = "${local.name}-nodes"
}

// Members of this group will have administrative access to the cluster.
resource "azuread_group" "admin" {
  display_name     = "${local.name}-admin"
  security_enabled = true
}

// Assign prerequisite role for cluster administrative group.
resource "azurerm_role_assignment" "list-users" {
  scope                = "/subscriptions/${local.subscription_id}"
  role_definition_name = "Azure Kubernetes Service Cluster User Role"
  principal_id         = azuread_group.admin.id
}

// Give cluster access to manage resources in the entire subscription.
// TODO: scope this more narrowly
// ref: https://learn.microsoft.com/en-us/azure/aks/use-managed-identity
resource "azurerm_user_assigned_identity" "self" {
  name                = "cluster"
  location            = azurerm_resource_group.env.location
  resource_group_name = azurerm_resource_group.env.name
}
resource "azurerm_role_assignment" "managed_identity_operator" {
  principal_id         = azurerm_user_assigned_identity.self.principal_id
  role_definition_name = "Managed Identity Operator"
  scope                = "/subscriptions/${local.subscription_id}"
}
resource "azurerm_role_assignment" "contributor" {
  principal_id         = azurerm_user_assigned_identity.self.principal_id
  role_definition_name = "Contributor"
  scope                = "/subscriptions/${local.subscription_id}"
}
resource "azurerm_role_assignment" "network_contributor" {
  principal_id         = azurerm_user_assigned_identity.self.principal_id
  role_definition_name = "Network Contributor"
  scope                = "/subscriptions/${local.subscription_id}"
}
