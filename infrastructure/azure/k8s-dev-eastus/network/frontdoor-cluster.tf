resource "azurerm_cdn_frontdoor_custom_domain" "cluster" {
  name                     = "${local.name}-ingress-cluster"
  cdn_frontdoor_profile_id = azurerm_cdn_frontdoor_profile.ingress.id
  dns_zone_id              = local.dns.zone.id
  host_name                = "${azurerm_dns_cname_record.ingress-cluster.name}.${azurerm_dns_cname_record.ingress-cluster.zone_name}"
  tls {
    certificate_type        = "CustomerCertificate"
    cdn_frontdoor_secret_id = azurerm_cdn_frontdoor_secret.cluster.id
    minimum_tls_version     = "TLS12"
  }
  depends_on = [
    azuread_service_principal.frontdoor
  ]
}

resource "azurerm_cdn_frontdoor_custom_domain" "cluster-wildcard" {
  name                     = "${local.name}-ingress-cluster-wildcard"
  cdn_frontdoor_profile_id = azurerm_cdn_frontdoor_profile.ingress.id
  dns_zone_id              = local.dns.zone.id
  host_name                = "*.${azurerm_dns_cname_record.ingress-cluster.name}.${azurerm_dns_cname_record.ingress-cluster.zone_name}"
  tls {
    certificate_type        = "CustomerCertificate"
    cdn_frontdoor_secret_id = azurerm_cdn_frontdoor_secret.cluster.id
    minimum_tls_version     = "TLS12"
  }
  depends_on = [
    azuread_service_principal.frontdoor
  ]
}