resource "azurerm_cdn_frontdoor_custom_domain" "storage" {
  name                     = "${local.name}-ingress-storage"
  cdn_frontdoor_profile_id = azurerm_cdn_frontdoor_profile.ingress.id
  dns_zone_id              = local.dns.zone.id
  host_name                = "${azurerm_dns_cname_record.ingress-storage.name}.${azurerm_dns_cname_record.ingress-storage.zone_name}"
  tls {
    certificate_type    = "ManagedCertificate"
    minimum_tls_version = "TLS12"
  }
}