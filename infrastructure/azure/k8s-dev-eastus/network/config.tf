locals {
  area      = "k8s"
  env       = "dev"
  region    = "eastus"
  workspace = "network"
  name      = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-${local.area}-${local.env}-${local.region}/${local.workspace}"
  }
  domain          = "k8s.dev.scaleout.online"
  tenant_id       = file("${path.module}/../../.config/azure-tenant-id")
  subscription_id = file("${path.module}/../../.config/azure-dev-subscription-id")
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.36.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "=4.0.4"
    }
    acme = {
      source  = "vancluever/acme"
      version = "=2.13.1"
    }
  }
}

provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
  subscription_id = local.subscription_id
}

provider "azuread" {}

provider "tls" {}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-network"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-network/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-k8s-dev-eastus-network/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

resource "azurerm_resource_group" "env" {
  name     = local.name
  location = local.region
  tags     = local.tags
}
