output "id" {
  value = azurerm_virtual_network.main.id
}

output "cidr" {
  value = local.cidr
}

output "subnets" {
  value = local.subnets
}

output "subnet_ids" {
  value = { for subnet in azurerm_subnet.main : subnet.name => subnet.id }
}

output "resource_group" {
  value = azurerm_resource_group.env
}

output "web_private_zone_id" {
  value = azurerm_private_dns_zone.web.id
}

output "mongo_private_zone_id" {
  value = azurerm_private_dns_zone.mongo.id
}

output "frontdoor_profile_id" {
  value = azurerm_cdn_frontdoor_profile.ingress.id
}

output "frontdoor_endpoint_id" {
  value = azurerm_cdn_frontdoor_endpoint.ingress.id
}

output "frontdoor_custom_domain_id_cluster" {
  value = azurerm_cdn_frontdoor_custom_domain.cluster.id
}

output "frontdoor_custom_domain_id_cluster_wildcard" {
  value = azurerm_cdn_frontdoor_custom_domain.cluster-wildcard.id
}

output "frontdoor_custom_domain_id_storage" {
  value = azurerm_cdn_frontdoor_custom_domain.storage.id
}