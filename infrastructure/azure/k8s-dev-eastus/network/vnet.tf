locals {
  cidr = "10.10.0.0/20"
  subnets = {
    "apps1"     = "10.10.0.0/21",
    "apps2"     = "10.10.8.0/22",
    "apps3"     = "10.10.12.0/23",
    "data"      = "10.10.14.0/24",
    "apiserver" = "10.10.15.0/25",
    "services"  = "10.10.15.128/26",
    // 10.10.15.192/27
    "routing" = "10.10.15.224/27"
  }
}

resource "azurerm_virtual_network" "main" {
  address_space       = [local.cidr]
  name                = local.name
  resource_group_name = azurerm_resource_group.env.name
  location            = azurerm_resource_group.env.location
  tags                = local.tags
}

resource "azurerm_subnet" "main" {
  for_each                                      = local.subnets
  virtual_network_name                          = azurerm_virtual_network.main.name
  address_prefixes                              = [each.value]
  name                                          = each.key
  resource_group_name                           = azurerm_resource_group.env.name
  private_link_service_network_policies_enabled = false
  lifecycle {
    // aks makes changes to this
    ignore_changes = [delegation]
  }
}
