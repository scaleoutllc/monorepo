resource "azurerm_dns_cname_record" "ingress-storage" {
  name                = "k8s-storage"
  zone_name           = local.dns.zone.name
  resource_group_name = local.dns.resource_group_name
  ttl                 = 3600
  record              = azurerm_cdn_frontdoor_endpoint.ingress.host_name
}

resource "azurerm_dns_txt_record" "ingress-storage-validate" {
  name                = "_dnsauth.k8s-storage"
  zone_name           = local.dns.zone.name
  resource_group_name = local.dns.zone.resource_group_name
  ttl                 = 3600
  record {
    value = azurerm_cdn_frontdoor_custom_domain.storage.validation_token
  }
}

