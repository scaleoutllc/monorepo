
data "azurerm_client_config" "current" {}

resource "azurerm_key_vault" "letsencrypt" {
  name                            = "${local.area}${local.env}${local.region}letsencrypt"
  location                        = azurerm_resource_group.env.location
  resource_group_name             = azurerm_resource_group.env.name
  sku_name                        = "premium"
  tenant_id                       = local.tenant_id
  enabled_for_disk_encryption     = true
  purge_protection_enabled        = false
  enabled_for_template_deployment = true
  enabled_for_deployment          = true
  soft_delete_retention_days      = 14

  # let terraform store certificates
  access_policy {
    tenant_id               = local.tenant_id
    object_id               = data.azurerm_client_config.current.object_id
    certificate_permissions = ["Get", "Import", "Delete", "Purge"]
    secret_permissions      = ["Get"]
  }
  # let front door access certificates
  access_policy {
    tenant_id          = local.tenant_id
    object_id          = azuread_service_principal.frontdoor.object_id
    secret_permissions = ["Get"]
  }
  network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }
}

resource "tls_private_key" "test" {
  algorithm = "RSA"
}

resource "acme_certificate" "cluster" {
  common_name               = local.domain
  subject_alternative_names = ["*.${local.domain}"]
  key_type                  = 4096
  account_key_pem           = local.users.letsencrypt.account_key_pem
  min_days_remaining        = 60 # Won't request new certificate until current certificate is > 29 days old
  recursive_nameservers     = ["8.8.8.8"]
  dns_challenge {
    provider = "azure"
    config = {
      AZURE_CLIENT_ID       = local.users.letsencrypt.client_id
      AZURE_CLIENT_SECRET   = local.users.letsencrypt.client_secret
      AZURE_RESOURCE_GROUP  = local.dns.resource_group_name
      AZURE_SUBSCRIPTION_ID = local.subscription_id
      AZURE_TENANT_ID       = local.tenant_id
      AZURE_ZONE_NAME       = "dev.scaleout.online"
      LEGO_DISABLE_CNAME_SUPPORT = true
    }
  }
}

resource "azurerm_key_vault_certificate" "cluster" {
  name         = "${replace(local.domain, ".", "-")}-wildcard-cert"
  key_vault_id = azurerm_key_vault.letsencrypt.id
  certificate {
    contents = acme_certificate.cluster.certificate_p12
    password = ""
  }
  certificate_policy {
    issuer_parameters {
      name = "Unknown"
    }
    key_properties {
      exportable = true
      reuse_key  = true
      key_size   = 4096
      key_type   = "RSA"
    }
    secret_properties {
      content_type = "application/x-pkcs12"
    }
  }
}

resource "azuread_service_principal" "frontdoor" {
  application_id = "205478c0-bd83-4e1b-a9d6-db63a3e1e1c8"
  use_existing   = true
}

resource "azurerm_cdn_frontdoor_secret" "cluster" {
  name                     = replace(local.domain, ".", "-")
  cdn_frontdoor_profile_id = azurerm_cdn_frontdoor_profile.ingress.id
  secret {
    customer_certificate {
      key_vault_certificate_id = azurerm_key_vault_certificate.cluster.id
    }
  }
}