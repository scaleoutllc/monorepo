resource "azurerm_dns_cname_record" "ingress-cluster" {
  name                = local.area
  zone_name           = local.dns.zone.name
  resource_group_name = local.dns.resource_group_name
  ttl                 = 3600
  record              = azurerm_cdn_frontdoor_endpoint.ingress.host_name
}

 Validation token was only created at front door domain initial creation. Destroying and recreating does not output
 a new validation token
resource "azurerm_dns_txt_record" "ingress-cluster-validate" {
  name                = "_dnsauth.${local.area}"
  zone_name           = local.dns.zone.name
  resource_group_name = local.dns.zone.resource_group_name
  ttl                 = 3600
  record {
    value = azurerm_cdn_frontdoor_custom_domain.cluster.validation_token
  }
}

resource "azurerm_dns_cname_record" "ingress-cluster-wildcard" {
  name                = "*.${local.area}"
  zone_name           = local.dns.zone.name
  resource_group_name = local.dns.resource_group_name
  ttl                 = 3600
  record              = azurerm_cdn_frontdoor_endpoint.ingress.host_name
}
