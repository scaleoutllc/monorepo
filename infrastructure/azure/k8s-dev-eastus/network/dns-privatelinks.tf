resource "azurerm_private_dns_zone" "web" {
  name                = "privatelink.web.core.windows.net"
  resource_group_name = azurerm_resource_group.env.name
  tags                = local.tags
}

resource "azurerm_private_dns_zone_virtual_network_link" "web" {
  name                  = "${local.name}-web"
  resource_group_name   = azurerm_resource_group.env.name
  private_dns_zone_name = azurerm_private_dns_zone.web.name
  virtual_network_id    = azurerm_virtual_network.main.id
}

resource "azurerm_private_dns_zone" "mongo" {
  name                = "privatelink.mongo.cosmos.azure.com"
  resource_group_name = azurerm_resource_group.env.name
  tags                = local.tags
}

resource "azurerm_private_dns_zone_virtual_network_link" "mongo" {
  name                  = "${local.name}-mongo"
  resource_group_name   = azurerm_resource_group.env.name
  private_dns_zone_name = azurerm_private_dns_zone.mongo.name
  virtual_network_id    = azurerm_virtual_network.main.id
}

