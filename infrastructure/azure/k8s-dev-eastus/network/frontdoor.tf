resource "azurerm_cdn_frontdoor_profile" "ingress" {
  name                = "${local.name}-ingress"
  resource_group_name = azurerm_resource_group.env.name
  sku_name            = "Premium_AzureFrontDoor"
  tags                = local.tags
}

resource "azurerm_cdn_frontdoor_endpoint" "ingress" {
  name                     = "${local.name}-ingress"
  cdn_frontdoor_profile_id = azurerm_cdn_frontdoor_profile.ingress.id
  enabled                  = true
  tags                     = local.tags
}

resource "azurerm_cdn_frontdoor_firewall_policy" "ingress" {
  name                = replace("${local.name}waf", "-", "")
  resource_group_name = azurerm_resource_group.env.name
  sku_name            = azurerm_cdn_frontdoor_profile.ingress.sku_name
  enabled             = true
  mode                = "Prevention"
  managed_rule {
    type    = "Microsoft_DefaultRuleSet"
    version = "2.0"
    action  = "Block"
  }
  managed_rule {
    type    = "Microsoft_BotManagerRuleSet"
    version = "1.0"
    action  = "Block"
  }
}

/*
resource "azurerm_log_analytics_workspace" "main" {
  name                = "${local.name}-log"
  location            = azurerm_resource_group.env.location
  resource_group_name = azurerm_resource_group.env.name
  sku                 = "PerGB2018"
  retention_in_days   = 30
  daily_quota_gb      = 30
  tags = local.tags
}

data "azurerm_monitor_diagnostic_categories" "frontdoor" {
  resource_id = azurerm_cdn_frontdoor_profile.main.id
}

resource "azurerm_monitor_diagnostic_setting" "main" {
  name                       = "${local.name}-afd"
  target_resource_id         = azurerm_cdn_frontdoor_profile.main.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.main.id

  dynamic "enabled_log" {
    iterator = entry
    for_each = data.azurerm_monitor_diagnostic_categories.frontdoor.log_category_types

    content {
      category = entry.value
      retention_policy {
        enabled = true
        days    = 30
      }
    }
  }

  dynamic "metric" {
    iterator = entry
    for_each = data.azurerm_monitor_diagnostic_categories.frontdoor.metrics

    content {
      category = entry.value
      enabled  = true

      retention_policy {
        enabled = true
        days    = 30
      }
    }
  }
}
*/