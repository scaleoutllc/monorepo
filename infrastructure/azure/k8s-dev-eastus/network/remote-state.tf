data "terraform_remote_state" "azure-shared-dev-eastus-dns" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-dns"
  }
}

data "terraform_remote_state" "azure-shared-dev-eastus-gitlab" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-gitlab"
  }
}

data "terraform_remote_state" "azure-users" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-users"
  }
}

locals {
  dns    = data.terraform_remote_state.azure-shared-dev-eastus-dns.outputs
  gitlab = data.terraform_remote_state.azure-shared-dev-eastus-gitlab.outputs
  users  = data.terraform_remote_state.azure-users.outputs
}