# Delegate control from root zone to dev environment
resource "azurerm_dns_ns_record" "delegate-dev" {
  provider            = azurerm.prod
  name                = "dev"
  resource_group_name = local.dns.root.resource_group_name
  zone_name           = local.dns.root.zone.name
  ttl                 = 30
  records             = local.dns.dev.zone.name_servers
  tags                = local.tags
}