data "terraform_remote_state" "azure-shared-prod-eastus" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-prod-eastus-dns"
  }
}

data "terraform_remote_state" "azure-shared-dev-eastus" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-dev-eastus-dns"
  }
}

locals {
  dns = {
    root = data.terraform_remote_state.azure-shared-prod-eastus.outputs
    dev  = data.terraform_remote_state.azure-shared-dev-eastus.outputs
  }
}