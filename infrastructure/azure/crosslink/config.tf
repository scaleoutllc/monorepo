locals {
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-crosslink"
  }
  subscriptions = {
    dev  = file("${path.module}/../.config/azure-dev-subscription-id")
    prod = file("${path.module}/../.config/azure-prod-subscription-id")
  }
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
  }
}

provider "azurerm" {
  alias           = "prod"
  subscription_id = local.subscriptions.prod
  features {}
}

provider "azurerm" {
  alias           = "dev"
  subscription_id = local.subscriptions.dev
  features {}
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-crosslink"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-crosslink/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-crosslink/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}