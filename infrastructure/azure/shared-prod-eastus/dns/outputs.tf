output "zone" {
  value = azurerm_dns_zone.main
}

output "resource_group_name" {
  value = azurerm_resource_group.env.name
}