data "azurerm_subscription" "current" {}

resource "azuread_application" "gitlab" {
  display_name = local.name
}

resource "azuread_service_principal" "gitlab" {
  application_id = azuread_application.gitlab.application_id
}

resource "azuread_service_principal_password" "gitlab" {
  service_principal_id = azuread_service_principal.gitlab.object_id
}

resource "azurerm_role_assignment" "subscription_owner" {
  scope                = data.azurerm_subscription.current.id
  role_definition_name = "Owner"
  principal_id         = azuread_service_principal.gitlab.id
}

locals {
  envs = {
    masked = {
      AZURE_PROD_CLIENT_SECRET = azuread_service_principal_password.gitlab.value
    }
    unmasked = {
      AZURE_PROD_SUBSCRIPTION_ID = local.subscription_id
      AZURE_PROD_CLIENT_ID       = azuread_application.gitlab.application_id
    }
  }
}

// TODO: convert to group variables so they cannot be viewed in the UI?
resource "gitlab_project_variable" "masked" {
  for_each = local.envs.masked
  project  = local.gitlab_project_id
  key      = each.key
  value    = each.value
  masked   = true
}

// TODO: convert to group variables so they cannot be viewed in the UI?
resource "gitlab_project_variable" "unmasked" {
  for_each = local.envs.unmasked
  project  = local.gitlab_project_id
  key      = each.key
  value    = each.value
  masked   = false
}