locals {
  area      = "shared"
  env       = "prod"
  region    = "eastus"
  workspace = "gitlab"
  name      = "${local.area}-${local.env}-${local.region}-${local.workspace}"
  tags = {
    terraform = "true"
    repo      = "monorepo"
    code_path = "infrastructure/azure-${local.area}-${local.env}-${local.region}/${local.workspace}"
  }
  tenant_id         = file("${path.module}/../../.config/azure-tenant-id")
  subscription_id   = file("${path.module}/../../.config/azure-prod-subscription-id")
  gitlab_project_id = file("${path.module}/../../../.config/gitlab-project-id")
}

terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "=2.36.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.46.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.10.0"
    }
  }
}

provider "azuread" {}
provider "azurerm" {
  features {}
  subscription_id = local.subscription_id
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-prod-eastus-gitlab"
    lock_address   = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-prod-eastus-gitlab/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45482900/terraform/state/azure-shared-prod-eastus-gitlab/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}