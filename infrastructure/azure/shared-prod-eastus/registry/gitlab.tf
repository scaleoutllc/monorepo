locals {
  envs = {
    masked = {
      AZURE_REGISTRY_PASSWORD = azuread_service_principal_password.registry.value
    }
    unmasked = {
      AZURE_REGISTRY      = azurerm_container_registry.main.login_server
      AZURE_REGISTRY_USER = azuread_application.registry.application_id
      DOCKER_AUTH_CONFIG  = "{\"auths\":{\"${azurerm_container_registry.main.login_server}\":{\"auth\":\"${base64encode("${azuread_application.registry.application_id}:${azuread_service_principal_password.registry.value}")}\"}}}"
    }
  }
}

// TODO: convert to group variables so they cannot be viewed in the UI?
resource "gitlab_project_variable" "azure-masked" {
  for_each = local.envs.masked
  project  = local.gitlab_project_id
  key      = each.key
  value    = each.value
  masked   = true
}

// TODO: convert to group variables so they cannot be viewed in the UI?
resource "gitlab_project_variable" "azure-unmasked" {
  for_each = local.envs.unmasked
  project  = local.gitlab_project_id
  key      = each.key
  value    = each.value
  masked   = false
}