resource "azurerm_container_registry" "main" {
  name                = "${local.area}${local.region}${local.workspace}"
  resource_group_name = azurerm_resource_group.env.name
  location            = azurerm_resource_group.env.location
  sku                 = "Premium"
}

resource "azuread_application" "registry" {
  display_name = "${local.area}${local.region}${local.workspace}"
}

resource "azuread_service_principal" "registry" {
  application_id = azuread_application.registry.application_id
}

resource "azuread_service_principal_password" "registry" {
  service_principal_id = azuread_service_principal.registry.object_id
}

resource "azurerm_role_assignment" "registry_owner" {
  scope                = azurerm_container_registry.main.id
  role_definition_name = "Owner"
  principal_id         = azuread_service_principal.registry.id
}