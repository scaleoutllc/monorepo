output "id" {
  value = azurerm_container_registry.main.id
}

output "host" {
  value = azurerm_container_registry.main.login_server
}

output "admin_client_id" {
  value = azuread_application.registry.application_id
}

output "admin_client_secret" {
  value     = azuread_service_principal_password.registry.value
  sensitive = true
}